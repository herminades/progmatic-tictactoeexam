/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.progmatic.tictactoeexam;

import com.progmatic.tictactoeexam.enums.PlayerType;
import com.progmatic.tictactoeexam.interfaces.AbstractPlayer;
import com.progmatic.tictactoeexam.interfaces.Board;

/**
 *
 * @author herminades
 */
public class SimplePlayer extends AbstractPlayer {

    public SimplePlayer(PlayerType p) {
        super(p);
    }

    @Override
    public Cell nextMove(Board b) {

        Cell c = null;

        if (b.emptyCells().size() > 0) {
            for (Cell emptyCell : b.emptyCells()) {
                c = emptyCell;
                c.setCellsPlayer(myType);
            }
        }
        return c;

    }
}
