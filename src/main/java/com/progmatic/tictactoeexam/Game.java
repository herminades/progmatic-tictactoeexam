/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.progmatic.tictactoeexam;

import com.progmatic.tictactoeexam.enums.PlayerType;
import com.progmatic.tictactoeexam.exceptions.CellException;
import com.progmatic.tictactoeexam.interfaces.Board;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author herminades
 */
public class Game implements Board {

    PlayerType[][] gameBoard = new PlayerType[3][3];

    public Game() {
        for (int i = 0; i < 3; i++) {
            for (int j = 0; j < 3; j++) {
                gameBoard[i][j] = PlayerType.EMPTY;
            }
        }
    }

    @Override
    public PlayerType getCell(int rowIdx, int colIdx) throws CellException {

        if (rowIdx < 0 || colIdx < 0 || rowIdx >= 3 || colIdx >= 3) {
            throw new CellException(rowIdx, colIdx, "Problem with the board-cell with coordinates: (" + rowIdx + ", " + colIdx + ").");
        }
        return gameBoard[rowIdx][colIdx];
    }

    @Override
    public void put(Cell cell) throws CellException {

        int row = cell.getRow();
        int col = cell.getCol();

        if (row < 0 || col < 0 || row >= 3 || col >= 3) {
            throw new CellException(row, col, "Problem with the board-cell with coordinates: (" + row + ", " + col + ").");
        }
        if (gameBoard[row][col] != PlayerType.EMPTY) {
            throw new CellException(row, col, "These cells are are not available: (" + row + ", " + col + ").");
        }

        gameBoard[row][col] = cell.getCellsPlayer();
    }

    @Override
    public boolean hasWon(PlayerType p) {

        if (gameBoard[0][0] == gameBoard[1][1] && gameBoard[1][1] == gameBoard[2][2] && gameBoard[1][1] == p) {
            return true;
        }
        if (gameBoard[0][2] == gameBoard[1][1] && gameBoard[1][1] == gameBoard[2][0] && gameBoard[1][1] == p) {
            return true;
        }

        for (int i = 0; i < 3; i++) {
            if (gameBoard[i][0] == gameBoard[i][1] && gameBoard[i][1] == gameBoard[i][2] && gameBoard[i][0] == p) {
                return true;
            }
        }
        for (int i = 0; i < 3; i++) {
            if (gameBoard[0][i] == gameBoard[1][i] && gameBoard[1][i] == gameBoard[2][i] && gameBoard[0][i] == p) {
                return true;
            }
        }
        return false;
    }

    @Override
    public List<Cell> emptyCells() {
        List<Cell> l = new ArrayList<>();
        for (int i = 0; i < 3; i++) {
            for (int j = 0; j < 3; j++) {
                if (gameBoard[i][j] == PlayerType.EMPTY) {
                    l.add(new Cell(i, j, PlayerType.EMPTY));
                }
            }
        }
        return l;
    }

}
